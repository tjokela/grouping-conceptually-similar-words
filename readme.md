NOTE: To view as a notebook:

Select conceptual_grouping.ipynb

In the top panel change "Default File Viewer" to "Ipython Notebook"

This is a brief test on how to group together (Finnish) words that share some conceptual connection. The test utilizes SOM maps and word embeddings to achieve to desired result.
